import 'package:shared_preferences/shared_preferences.dart';

class AppPreferences {
  static final AppPreferences _instance = AppPreferences._internal();
  late SharedPreferences _sharedPreferences;

  factory AppPreferences() {
    return _instance;
  }

  AppPreferences._internal();

  Future<void> initPreferences() async {
    _sharedPreferences = await SharedPreferences.getInstance();
  }

  Future<void> setEmail(String email) async{
    await _sharedPreferences.setString('email', email);
  }

  String getEmail() {
    return _sharedPreferences.getString('email') ?? '';
  }

  Future<void> setPassword(String password) async{
    await _sharedPreferences.setString('password', password);
  }

  String getPassword() {
    return _sharedPreferences.getString('password') ?? '';
  }

  Future<void> setMarketName(String marketName) async{
    await _sharedPreferences.setString('marketName', marketName);
  }

  Future<void> setMarketId(String marketId) async{
    await _sharedPreferences.setString('marketId', marketId);
  }

  String getMarketId() {
    return _sharedPreferences.getString('marketId') ?? '';
  }

  String getMarketName() {
    return _sharedPreferences.getString('marketName') ?? '';
  }

  Future<void> setCategory(String category) async{
    await _sharedPreferences.setString('category', category);
  }

  String getCategory() {
    return _sharedPreferences.getString('category') ?? '';
  }

  Future<void> setAmountPaid(int amountPaid) async{
    await _sharedPreferences.setInt('amountPaid', amountPaid);
  }

  int getAmountPaid() {
    return _sharedPreferences.getInt('amountPaid') ?? 0;
  }

  Future<void> setTotalPrice(int total) async{
    await _sharedPreferences.setInt('total', total);
  }

  int getTotalPrice() {
    return _sharedPreferences.getInt('total') ?? 0;
  }

}
