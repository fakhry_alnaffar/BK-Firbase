import 'dart:io';

import 'package:bio_clean/firebase/fb_auth_controller.dart';
import 'package:bio_clean/firebase/fb_firestore.dart';
import 'package:bio_clean/firebase/firebase_storage.dart';
import 'package:bio_clean/mod/admin.dart';
import 'package:bio_clean/mod/employee.dart';
import 'package:bio_clean/preferences/student_pref.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:bio_clean/utils/helpers.dart';
import 'package:bio_clean/widgets/app_text_filed.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:future_progress_dialog/future_progress_dialog.dart';
import 'package:image_picker/image_picker.dart';

class AddEmployeeScreen extends StatefulWidget {
  final Admin? employee;

  AddEmployeeScreen({required this.employee});

  @override
  _AddEmployeeState createState() => _AddEmployeeState();
}

class _AddEmployeeState extends State<AddEmployeeScreen> with Helpers {
  XFile? pickedImage;
  XFile? pickedImage2;
  ImagePicker imagePicker = ImagePicker();
  String selectedImage =
      'https://image.flaticon.com/icons/png/512/314/314479.png';
  late TextEditingController _nameController;
  late TextEditingController _emailController;
  late TextEditingController _passwordController;
  late bool isPassword = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController = TextEditingController(
        text: widget.employee != null ? widget.employee!.name : '');
    _emailController = TextEditingController(
        text: widget.employee != null ? widget.employee!.email : '');
    _passwordController =
        TextEditingController(text: widget.employee != null ? ' ' : '');
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _nameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            '${widget.employee != null ? 'Employee Details' : 'Add Employee'}',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
        actions: [
          Visibility(
            visible: widget.employee != null,
            child: IconButton(
                onPressed: () {
                  deleteDistributorFromAuth();
                  deleteDistributor();
                },
                icon: Icon(
                  Icons.delete_outline,
                  color: Colors.white,
                )),
          )
        ],
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xffED2424),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: 100),
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: SingleChildScrollView(
                    padding: EdgeInsetsDirectional.only(top: 40),
                    clipBehavior: Clip.antiAlias,
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        Visibility(
                          visible: widget.employee != null,
                          child: SizedBox(
                            height: SizeConfig().scaleHeight(10),
                          ),
                        ),
                        Visibility(
                          visible: widget.employee != null,
                          child: GestureDetector(
                            onTap: () {
                              // pickImage();
                            },
                            child: Container(
                              clipBehavior: Clip.antiAlias,
                              width: 100,
                              height: 100,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Color(0xffED2424),
                              ),
                              child: widget.employee != null
                                  ? pickedImage2 != null
                                      ? Image.file(
                                          File(pickedImage2!.path),
                                          fit: BoxFit.cover,
                                        )
                                      : Image.network(
                                          selectedImage,
                                          fit: BoxFit.cover,
                                        )
                                  : pickedImage2 != null
                                      ? Image.file(
                                          File(pickedImage2!.path),
                                          fit: BoxFit.cover,
                                        )
                                      : Icon(
                                          Icons.add_photo_alternate_outlined,
                                          color: Colors.white,
                                          size: 50,
                                        ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(30),
                        ),
                        AppTextFiled(
                          labelText: 'Name',
                          controller: _nameController,
                          prefix: Icons.person,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(20),
                        ),
                        // AppTextFiled(
                        //   textInputType: TextInputType.number,
                        //   labelText: 'Mobile',
                        //   controller: _mobileNumberController,
                        //   prefix: Icons.phone,
                        // ),
                        // SizedBox(
                        //   height: SizeConfig().scaleHeight(20),
                        // ),
                        AppTextFiled(
                          // enabled: widget.distributor != null ? false : true,
                          readOnly: widget.employee != null ? true : false,
                          showCursor: widget.employee != null ? false : true,
                          textInputType: TextInputType.emailAddress,
                          labelText: 'Email',
                          controller: _emailController,
                          prefix: Icons.email,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(20),
                        ),
                        Visibility(
                          visible: widget.employee == null,
                          child: AppTextFiled(
                            // enabled: widget.distributor != null ? false : true,
                            readOnly: widget.employee != null ? true : false,
                            showCursor: widget.employee != null ? false : true,
                            labelText: 'Password',
                            controller: _passwordController,
                            obscureText: isPassword,
                            prefix: Icons.lock,
                            suffix: isPassword
                                ? Icons.visibility
                                : Icons.visibility_off,
                            functionSuffixPressed: () {
                              setState(() {
                                isPassword = !isPassword;
                              });
                            },
                          ),
                        ),
                        Visibility(
                          visible: widget.employee == null,
                          child: SizedBox(
                            height: SizeConfig().scaleHeight(30),
                          ),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            minimumSize: Size(
                                double.infinity, SizeConfig().scaleHeight(60)),
                            primary: Color(0xffED2424),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          onPressed: () async {
                            bool s = await signUp();
                            if (s == true) {
                              print('success');
                              Navigator.pop(context);
                            } else {
                              print('error');
                            }
                          },
                          child: Text(
                            widget.employee != null ? 'Edit' : 'Add',
                            style: TextStyle(
                                fontSize: SizeConfig().scaleTextFont(20)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  bool checkData() {
    if (_emailController.text.isNotEmpty &&
        _nameController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty) {
      return true;
    } else {
      showSnackBar(
          context: context,
          message: 'One or more input are empty',
          error: true);
      return false;
    }
  }

  Future<bool> signUp() async {
    if (checkData()) {
      if (widget.employee != null) {
        return await updateDistributor();
      } else {
        return await createAccountProces();
      }
    }
    return false;
  }

  Future<bool> createAccountProces() async {
    await FirebaseAuth.instance.signOut();
    bool state = await FbAuthController().createAccount(context,
        email: _emailController.text, password: _passwordController.text);
    if (state) {
      await FbFireStoreController().addEmployee(employee: distributor);
      await FirebaseAuth.instance.signOut();
      await FbAuthController().signIn(context,
          email: AppPreferences().getEmail(),
          password: AppPreferences().getPassword());
      // Navigator.pop(context);
    }
    return state;
  }

  Future<bool> updateDistributor() async {
    bool state = await FbFireStoreController()
        .updateEmployee(path: widget.employee!.path, employee: distributor);
    if (state) {
      // Navigator.pop(context);
    }
    return state;
  }

  Future<void> deleteDistributor() async {
    bool state = await FbFireStoreController()
        .deleteEmployee(path: widget.employee!.path);
    if (state) {
      Navigator.pop(context);
    }
  }

  Future<void> deleteDistributorFromAuth() async {
    var state = await FbAuthController().deleteUser();
  }

  Future<void> pickImage() async {
    pickedImage = await imagePicker.pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      showDialog(
        context: context,
        builder: (context) => FutureProgressDialog(uploadImage(),
            message: Text('Uploading image...')),
      );
      setState(() {
        pickedImage2 = pickedImage;
      });
    }
  }

  Future<void> uploadImage() async {
    selectedImage = await FirebaseStorageController()
        .uploadImagee(File(pickedImage!.path), 'employee');
  }

  Admin get distributor {
    Admin distributor = Admin();
    distributor.name = _nameController.text;
    distributor.email = _emailController.text;
    distributor.image = selectedImage;
    return distributor;
  }
}
//0xff5A55CA
