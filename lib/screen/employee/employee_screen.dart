import 'package:bio_clean/components/component.dart';
import 'package:bio_clean/firebase/fb_firestore.dart';
import 'package:bio_clean/mod/admin.dart';
import 'package:bio_clean/mod/employee.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:bio_clean/screen/employee/add_employee.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class EmployeeScreen extends StatefulWidget {
  @override
  _DistributorsScreenState createState() => _DistributorsScreenState();
}

class _DistributorsScreenState extends State<EmployeeScreen> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pushReplacementNamed(context, 'home_screen');
              },
            )),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'EMPLOYEE',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            clipBehavior: Clip.antiAlias,
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xffED2424),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              clipBehavior: Clip.antiAlias,
              margin: EdgeInsets.only(top: 100),
              width: 414,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: StreamBuilder<QuerySnapshot>(
                  stream: FbFireStoreController().readEmployee(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      List<DocumentSnapshot> documents = snapshot.data!.docs;
                      return Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 24, vertical: 0),
                        child: ListView.separated(
                          padding: EdgeInsetsDirectional.only(top: 50),
                          itemBuilder: (context, index) {
                            return InkWell(
                              child: showDistributors(
                                documents: documents,
                                index: index,
                                context: context,
                                title: documents[index].get('name'),
                                subtitle: documents[index]
                                    .get('email')
                                    .toString(),
                                url: 'https://banner2.cleanpng.com/20180613/uz/kisspng-clerk-computer-icons-clip-art-clerk-5b20ba415e19a0.6115672115288714893854.jpg',
                              ),
                              onTap: () {
                                print(index);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => AddEmployeeScreen(
                                        employee:
                                            getDistributor(documents[index]),
                                      ),
                                    ));
                              },
                            );
                          },
                          separatorBuilder: (BuildContext context, int index) {
                            return SizedBox(
                              height: SizeConfig().scaleHeight(14),
                            );
                          },
                          itemCount: documents.length,
                          shrinkWrap: false,
                          physics: BouncingScrollPhysics(),
                          clipBehavior: Clip.antiAlias,
                        ),
                      );
                    } else {
                      return Center(
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.warning_amber_outlined,
                                color: Colors.grey,
                                size: 50,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'No items found',
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 16,
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                  }),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddEmployeeScreen(employee: null),
              ));
        },
        child: Icon(Icons.add),
        backgroundColor: Color(0xffED2424),
      ),
    );
  }

  Admin getDistributor(DocumentSnapshot snapshot) {
    Admin employee = Admin();
    employee.name = snapshot.get('name');
    employee.email = snapshot.get('email');
    employee.path = snapshot.id;
    employee.image = snapshot.get('image');
    return employee;
  }
}
