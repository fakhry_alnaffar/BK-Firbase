import 'package:bio_clean/firebase/fb_auth_controller.dart';
import 'package:bio_clean/firebase/fb_firestore.dart';
import 'package:bio_clean/preferences/student_pref.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:bio_clean/utils/helpers.dart';
import 'package:bio_clean/widgets/app_text_filed.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with Helpers {
  late TextEditingController _emailTextController;
  late TextEditingController _passwordTextController;
  late bool isPassword = true;
  late var formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _emailTextController = TextEditingController();
    _passwordTextController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _emailTextController.dispose();
    _passwordTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(0),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'LOGIN',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xffED2424),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: SizeConfig().scaleHeight(120)),
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(SizeConfig().scaleWidth(40)),
                  topRight: Radius.circular(SizeConfig().scaleWidth(40)),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: SingleChildScrollView(
                    padding: EdgeInsetsDirectional.only(top: 10),
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        Align(
                          alignment: AlignmentDirectional.topStart,
                          child: Padding(
                            padding: const EdgeInsetsDirectional.only(
                                start: 10, top: 10, bottom: 0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Welcome Back ...',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: SizeConfig().scaleTextFont(22),
                                    color: Colors.black,
                                  ),
                                  textAlign: TextAlign.start,
                                ),
                                SizedBox(
                                  height: SizeConfig().scaleHeight(5),
                                ),
                                Text(
                                  'Enter Email and Password..',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: SizeConfig().scaleTextFont(16),
                                      color: Colors.grey.shade500),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(30),
                        ),
                        AppTextFiled(
                          labelText: 'Email',
                          controller: _emailTextController,
                          textInputType: TextInputType.emailAddress,
                          maxLength: 50,
                          prefix: Icons.email,
                          validator: (email) {},
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(20),
                        ),
                        AppTextFiled(
                          labelText: 'Password',
                          controller: _passwordTextController,
                          obscureText: isPassword,
                          prefix: Icons.lock,
                          suffix: isPassword
                              ? Icons.visibility
                              : Icons.visibility_off,
                          functionSuffixPressed: () {
                            setState(() {
                              isPassword = !isPassword;
                            });
                          },
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(30),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            minimumSize: Size(
                                double.infinity, SizeConfig().scaleHeight(60)),
                            primary: Color(0xffED2424),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          onPressed: () {
                            performSignIn();
                            // Navigator.pushReplacementNamed(context, 'home_screen');
                          },
                          child: Text(
                            'LOGIN',
                            style: TextStyle(
                                fontSize: SizeConfig().scaleTextFont(18)),
                          ),
                        ),
                        Align(
                          alignment: AlignmentDirectional.centerEnd,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              SizedBox(height: 10),
                              SizedBox(
                                height: 40,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    TextButton(
                                      onPressed: () => Navigator.pushNamed(
                                          context, 'crate_account'),
                                      child: Text(
                                        'CREATE ACCOUNT',
                                        style:
                                            TextStyle(color: Color(0xffED2424)),
                                      ),
                                    ),
                                    VerticalDivider(
                                      thickness: 1,
                                      indent: 8,
                                      endIndent: 8,
                                      color: Colors.grey.shade500,
                                      width: 1,
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        Navigator.pushNamed(
                                            context, 'forgot_password');
                                        print('persed forget');
                                      },
                                      child: Text(
                                        'FORGET PASSWORD?',
                                        style:
                                            TextStyle(color: Color(0xffED2424)),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> performSignIn() async {
    if (checkData()) {
      await signIn();
    }
  }

  bool checkData() {
    if (_emailTextController.text.isNotEmpty &&
        _passwordTextController.text.isNotEmpty) {
      return true;
    }
    showSnackBar(context: context, message: 'Enter Required Data', error: true);
    return false;
  }

  Future<void> signIn() async {
    bool status = await FbAuthController().signIn(context,
        email: _emailTextController.text,
        password: _passwordTextController.text);
    if (status) {
      bool admin =
          await FbFireStoreController().isAdmin(_emailTextController.text);
      print(admin);
      if (admin) {
        await AppPreferences().setEmail(_emailTextController.text);
        await AppPreferences().setPassword(_passwordTextController.text);
        Navigator.pushReplacementNamed(context, 'home_screen');
      } else {
        showSnackBar(
            context: context, message: 'You are not admin', error: true);
      }
    }
  }
}
