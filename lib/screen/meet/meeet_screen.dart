import 'package:bio_clean/components/component.dart';
import 'package:bio_clean/firebase/fb_firestore.dart';
import 'package:bio_clean/mod/meet.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:bio_clean/screen/meet/edit_meet.dart';
import 'package:bio_clean/utils/helpers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class MeetScreen extends StatefulWidget {
  @override
  _MeetScreenState createState() => _MeetScreenState();
}

class _MeetScreenState extends State<MeetScreen> with Helpers {
  // MeetGetsController _meetGetsController = Get.put(MeetGetsController());
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // _meetGetsController.getMeet();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: Container(
              margin: EdgeInsetsDirectional.only(
                  top: SizeConfig().scaleWidth(0),
                  bottom: SizeConfig().scaleWidth(0),
                  start: SizeConfig().scaleWidth(5)),
              child: IconButton(
                icon: Icon(Icons.arrow_back_ios_rounded),
                onPressed: () {
                  Navigator.pushReplacementNamed(context, 'home_screen');
                },
              )),
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(20),
                bottom: SizeConfig().scaleWidth(0),
                end: SizeConfig().scaleWidth(0)),
            child: Text(
              'MEETS',
              style: TextStyle(
                  letterSpacing: 2,
                  wordSpacing: 0.5,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig().scaleWidth(24)),
            ),
          ),
        ),
        body: Stack(children: [
          Container(
            clipBehavior: Clip.antiAlias,
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xffED2424),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
              child: Container(
            clipBehavior: Clip.antiAlias,
            margin: EdgeInsets.only(top: SizeConfig().scaleHeight(100)),
            padding: EdgeInsetsDirectional.only(top: 0),
            width: double.infinity,
            height: double.infinity,
            alignment: Alignment.bottomCenter,
            decoration: BoxDecoration(
              color: Color(0xffF0F4FD),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(SizeConfig().scaleWidth(40)),
                topRight: Radius.circular(SizeConfig().scaleWidth(40)),
              ),
            ),
            child: StreamBuilder<QuerySnapshot>(
                stream: FbFireStoreController().readMeet(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<DocumentSnapshot> documents = snapshot.data!.docs;
                    return Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 24, vertical: 0),
                      child: ListView.separated(
                        padding: EdgeInsetsDirectional.only(top: 50),
                        itemBuilder: (context, index) {
                          return InkWell(
                            onLongPress: () {
                              showDialogFunction(document: documents[index].id);
                            },
                            child: showWidgetMeet(
                              description: documents[index].get('description'),
                              onDelete: IconButton(
                                onPressed: () async {},
                                icon: Icon(Icons.delete_rounded,
                                    size: SizeConfig().scaleHeight(26),
                                    color: Colors.transparent),
                              ),
                              status: documents[index].get('done') == '0'
                                  ? 'Initialize'
                                  : 'DONE',
                              time: documents[index].get('time'),
                              onPressed: () => Navigator.pop(context),
                              documents: documents,
                              index: index,
                              context: context,
                              title: documents[index].get('name'),
                              date: documents[index].get('date'),
                            ),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => EditMeetScreen(
                                      meet: getMeet(documents[index]),
                                    ),
                                  ));
                            },
                          );
                        },
                        separatorBuilder: (BuildContext context, int index) {
                          return SizedBox(
                            height: SizeConfig().scaleHeight(14),
                          );
                        },
                        itemCount: documents.length,
                        shrinkWrap: false,
                        physics: BouncingScrollPhysics(),
                        clipBehavior: Clip.antiAlias,
                      ),
                    );
                  } else {
                    return Center(
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.warning_amber_outlined,
                              color: Colors.grey,
                              size: 50,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              'NO MEETS',
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 16,
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }
                }),
          ))
        ]),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, 'add_meet');
          },
          child: Icon(Icons.add),
          backgroundColor: Color(0xffED2424),
        ),
      ),
    );
  }

  Future<void> delete({required String path}) async {
    bool status = await FbFireStoreController().deleteMeet(path: path);
    if (status) {
      showSnackBar(context: context, message: 'Meet Deleted Successfully');
    }
  }

  Meet getMeet(DocumentSnapshot snapshot) {
    Meet meet = Meet();
    meet.path = snapshot.id;
    meet.name = snapshot.get('name');
    meet.description = snapshot.get('description');
    meet.date = snapshot.get('date');
    meet.time = snapshot.get('time');
    meet.done = snapshot.get('done');
    return meet;
  }

  void showDialogFunction({required String document}) {
    showDialog(
        barrierColor: Colors.black.withOpacity(0.16),
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Confirm Delete'),
            content: Text('Are Your Sure!'),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    delete(path: document);
                  },
                  child: Text('YES')),
              TextButton(
                  onPressed: () => Navigator.pop(context), child: Text('NO')),
            ],
// actionsPadding: EdgeInsets.symmetric(horizontal: 50),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
          );
        });
  }
}
