import 'package:bio_clean/firebase/fb_firestore.dart';
import 'package:bio_clean/mod/meet.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:bio_clean/utils/helpers.dart';
import 'package:bio_clean/widgets/app_text_filed.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class EditMeetScreen extends StatefulWidget {
  final Meet? meet;
  EditMeetScreen({required this.meet});
  @override
  _EditMeetState createState() => _EditMeetState();
}

class _EditMeetState extends State<EditMeetScreen> with Helpers {
  late var _meetNameController;
  late var _meetTimeController;
  late var _meetDateController;
  late var _meetDescriptionController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _meetNameController = TextEditingController(text: widget.meet!.name);
    _meetTimeController = TextEditingController(text: widget.meet!.time);
    _meetDateController = TextEditingController(text: widget.meet!.date);
    _meetDescriptionController = TextEditingController(text: widget.meet!.description);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _meetNameController.dispose();
    _meetTimeController.dispose();
    _meetDateController.dispose();
    _meetDescriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          actions: [
            IconButton(
              onPressed: () {
                save();
              },
              icon: Icon(
                Icons.check_outlined,
                color: Colors.white,
              ),
            )
          ],
          centerTitle: true,
          elevation: 0,
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          title: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(20),
                bottom: SizeConfig().scaleWidth(10),
                end: SizeConfig().scaleWidth(0)),
            child: Text(
              ' ADD MEET',
              style: TextStyle(
                  letterSpacing: 2,
                  wordSpacing: 0.5,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig().scaleWidth(24)),
            ),
          ),
        ),
        body: Stack(
          children: [
            Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: AlignmentDirectional.topStart,
                  end: AlignmentDirectional.bottomEnd,
                  colors: [
                    Color(0xffED2424),
                    Colors.white,
                  ],
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(140),
            ),
            Align(
              child: Container(
                margin: EdgeInsets.only(top: SizeConfig().scaleHeight(100)),
                width: 414,
                height: double.infinity,
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                  color: Color(0xffF0F4FD),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          SizedBox(
                            height: SizeConfig().scaleHeight(40),
                          ),
                          AppTextFiled(
                            labelText: 'Meet Name',
                            controller: _meetNameController,
                            prefix: Icons.title,
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            textInputType: TextInputType.datetime,
                            labelText: 'Meet Time',
                            // isEnabled: false,
                            controller: _meetTimeController,
                            prefix: Icons.timer,
                            onTap: () {
                              showTimePicker(
                                context: context,
                                initialTime: TimeOfDay.now(),
                              ).then((value) => _meetTimeController.text =
                                  value!.format(context).toString());
                            },
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            textInputType: TextInputType.datetime,
                            labelText: 'Meet Date',
                            // isEnabled: false,
                            controller: _meetDateController,
                            prefix: Icons.date_range_rounded,
                            onTap: () {
                              showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime.now(),
                                lastDate:
                                DateTime.now().add(Duration(days: 60)),
                              ).then((value) {
                                // dateController.text=value.toString();
                                //باقي الشغل
                                _meetDateController.text = DateFormat.yMMMd()
                                    .format(value!)
                                    .toString();
                                // print(DateFormat.yMMMd().format(value!));
                              });
                            },
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            labelText: 'Meet Description',
                            controller: _meetDescriptionController,
                            prefix: Icons.description_rounded,
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool checkData() {
    if (
    _meetNameController.text.isNotEmpty &&
        _meetTimeController.text.isNotEmpty &&
        _meetDateController.text.isNotEmpty &&
        _meetDescriptionController.text.isNotEmpty) {
      return true;
    } else {
      showSnackBar(
          context: context, message: 'Enter Required Data', error: true);
      return false;
    }
  }

  Future<void> save() async {
    if (checkData()) {
      await updateMeet();
    }
  }

  Future<void> updateMeet() async {
    bool state = await FbFireStoreController().updateMeet(meet: meet,path: widget.meet!.path);
    if (state) {
      Navigator.pop(context);
    }
  }

  void clear() {
    _meetNameController.text = '';
    _meetTimeController.text = '';
    _meetDateController.text = '';
    _meetDescriptionController.text = '';
  }

  Meet getMeet(DocumentSnapshot snapshot) {
    Meet meet = Meet();
    meet.path = snapshot.id;
    meet.name = snapshot.get('name');
    meet.description = snapshot.get('description');
    meet.time = snapshot.get('time');
    meet.date = snapshot.get('date');
    return meet;
  }

  Meet get meet {
    Meet meet = Meet();
    meet.name = _meetNameController.text;
    meet.time = _meetTimeController.text;
    meet.date = _meetDateController.text;
    meet.description = _meetDescriptionController.text;
    meet.done = '0';
    return meet;
  }

}
//0xff5A55CA
