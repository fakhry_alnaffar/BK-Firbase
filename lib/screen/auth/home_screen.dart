import 'package:bio_clean/components/component.dart';
import 'package:bio_clean/firebase/fb_auth_controller.dart';
import 'package:bio_clean/firebase/fb_firestore.dart';
import 'package:bio_clean/mod/admin.dart';
import 'package:bio_clean/mod/dashboard_items.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  List<DashboardItem> items = <DashboardItem>[];
  int _countMeet = 0;
  int _countTask = 0;
  int _countUser = 0;
  int _countImportTask = 0;
  String name = '';
  late User _user;

  @override
  void initState() {
    // TODO: implement initState
    WidgetsBinding.instance!.addObserver(this);
    listValue();
    getItemCount();
    getUserName();
    _user = FbAuthController().user;
    setState(() {
      listValue();
      getItemCount();
      getUserName();
      _user = FbAuthController().user;
    });
    super.initState();
    getUserName();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      //do your stuff
      listValue();
      getUserName();
      setState(() {
        listValue();
        getUserName();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    items = <DashboardItem>[
      DashboardItem(
          title: 'Tasks',
          counterAndType: ' Task ${_countTask.toString()} ',
          icon: Icons.article_rounded,
          iconColor: Color(0xffF26950)),
      DashboardItem(
          title: 'Meet',
          counterAndType: ' Meet ${_countMeet.toString()}',
          icon: Icons.meeting_room_rounded,
          iconColor: Color(0xff30e208)),
      DashboardItem(
          title: 'Employees',
          counterAndType: ' Employee ${_countUser.toString()}',
          icon: Icons.person,
          iconColor: Color(0xff6e59e5)),
      DashboardItem(
          title: 'Import Tasks',
          counterAndType: '0 Import Task',
          icon: Icons.import_contacts_outlined,
          iconColor: Colors.redAccent),
    ];
    return Scaffold(
      endDrawerEnableOpenDragGesture: true,
      drawerEdgeDragWidth: 10,
      drawerEnableOpenDragGesture: true,
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
              icon: Container(
                margin: EdgeInsets.symmetric(vertical: 5),
                child: Image(
                  image: AssetImage(
                    "images/aa.png",
                  ),
                ),
              ),
            );
          },
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(0),
              bottom: SizeConfig().scaleWidth(0),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'Dashboard',
            style: TextStyle(
                letterSpacing: SizeConfig().scaleWidth(2),
                wordSpacing: SizeConfig().scaleWidth(0.5),
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      drawer: ClipRRect(
        borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(20.0),
            topRight: Radius.circular(20)),
        child: Container(
          color: Color(0xffED2424),
          width: 310,
          child: Drawer(
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: AlignmentDirectional.topStart,
                  end: AlignmentDirectional.bottomEnd,
                  colors: [
                    Color(0xffED2424),
                    Color(0xffED2424),
                    Color(0x6fed2424),
                  ],
                ),
              ),
              child: ListView(
                clipBehavior: Clip.antiAliasWithSaveLayer,
                physics: NeverScrollableScrollPhysics(),
                padding: EdgeInsets.symmetric(vertical: 10),
                primary: true,
                children: [
                  UserAccountsDrawerHeader(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(0),
                      gradient: LinearGradient(
                        begin: AlignmentDirectional.topStart,
                        end: AlignmentDirectional.bottomEnd,
                        colors: [
                          Color(0xffED2424),
                          Color(0xffED2424),
                          Color(0x6fed2424),
                        ],
                      ),
                    ),
                    accountName: Text(name),
                    accountEmail: Text('${_user.email}'),
                    currentAccountPicture: CircleAvatar(
                      backgroundColor: Colors.white,
                      backgroundImage: NetworkImage(
                          'https://banner2.cleanpng.com/20180613/uz/kisspng-clerk-computer-icons-clip-art-clerk-5b20ba415e19a0.6115672115288714893854.jpg'),
                    ),
                    arrowColor: Colors.black,
                  ),
                  Divider(
                    color: Colors.grey,
                    endIndent: 5,
                    indent: 0,
                    thickness: 1.5,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  ListTile(
                    // isThreeLine: true,
                    dense: true,
                    selected: false,
                    leading: Icon(
                      Icons.settings_applications,
                      size: 30,
                      color: Colors.white,
                    ),
                    title: Container(
                        margin: EdgeInsetsDirectional.only(bottom: 0),
                        padding: EdgeInsetsDirectional.only(bottom: 0),
                        child: Text(
                          'Setting',
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        )),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.pushNamed(context, 'profile_screen');
                    },
                  ),
                  SizedBox(height: SizeConfig().scaleHeight(10)),
                  ListTile(
                    dense: true,
                    selected: false,
                    leading: Icon(
                      Icons.login_outlined,
                      size: 30,
                      color: Colors.white,
                    ),
                    title: Container(
                        margin: EdgeInsetsDirectional.only(bottom: 0),
                        child: Text(
                          'Logout',
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        )),
                    onTap: () {
                      FbAuthController().signOut(context);
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xffED2424),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: SizeConfig().scaleHeight(100)),
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(SizeConfig().scaleWidth(40)),
                  topRight: Radius.circular(SizeConfig().scaleWidth(40)),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: GridView.builder(
                    itemCount: items.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: SizeConfig().scaleHeight(24),
                        crossAxisSpacing: SizeConfig().scaleHeight(24),
                        childAspectRatio: SizeConfig().scaleHeight(171) /
                            SizeConfig().scaleWidth(171),
                        mainAxisExtent: SizeConfig().scaleHeight(171)),
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          if (index == 0) {
                            print('TASK');
                            Navigator.pushReplacementNamed(
                                context, 'task_screen');
                          } else if (index == 1) {
                            print('MEET');
                            Navigator.pushReplacementNamed(
                                context, 'meet_screen');
                          } else if (index == 2) {
                            Navigator.pushReplacementNamed(
                                context, 'employee_screen');
                            print('EMPLOYEE');
                          } else if (index == 3) {
                            // Navigator.pushReplacementNamed(
                            //     context, 'category_home');
                            // print('IMPORT TASK');
                          } else if (index == 4) {
                            // Navigator.pushReplacementNamed(
                            //     context, 'bills_show');
                            // print('PROFILE');
                          }
                        },
                        child: homeWidget(
                          icon: items[index].icon,
                          counterAndType: items[index].counterAndType,
                          tittle: items[index].title,
                          iconColor: items[index].iconColor,
                        ),
                      );
                    }),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void listValue() {
    DashboardItem(
        title: 'Task',
        counterAndType: ' Task ${_countTask.toString()} ',
        icon: Icons.article_rounded,
        iconColor: Color(0xffF26950));
    DashboardItem(
        title: 'Meet',
        counterAndType: ' Meet ${_countMeet.toString()} ',
        icon: Icons.meeting_room_rounded,
        iconColor: Color(0xff30e208));
    DashboardItem(
        title: 'Employees',
        counterAndType: ' Employee ${_countUser.toString()}',
        icon: Icons.person,
        iconColor: Color(0xff6e59e5));
    DashboardItem(
        title: 'Import Tasks',
        counterAndType: '0 Import Task',
        icon: Icons.import_contacts_outlined,
        iconColor: Colors.redAccent);
  }

  Future<void> getEmployeeCount() async {
    int count = await FbFireStoreController().readEmployeeCount();
    setState(() {
      _countUser = count;
    });
  }

  Future<void> getTaskCount() async {
    int count = await FbFireStoreController().readTaskCount();
    setState(() {
      _countTask = count;
    });
  }

  Future<void> getMeetCount() async {
    int count = await FbFireStoreController().readMeetCount();
    setState(() {
      _countMeet = count;
    });
  }

  Future<void> getUserName() async {
    String namee = await FbFireStoreController().getAdminName();
    setState(() {
      name = namee;
      print('Name ' + name);
    });
  }

  void getItemCount() {
    // getCustomerCount();
    // getEmployeeCount();
    getTaskCount();
    getEmployeeCount();
    getMeetCount();
    // getOrderCount();
    // getProductCount();
    // getBillsCount();
  }

  Admin getAdmin() {
    Admin admin = Admin();
    admin.name = name;
    admin.email = _user.email.toString();
    return admin;
  }
}
