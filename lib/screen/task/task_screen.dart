import 'package:bio_clean/components/component.dart';
import 'package:bio_clean/firebase/fb_firestore.dart';
import 'package:bio_clean/mod/task.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:bio_clean/screen/task/edit_task.dart';
import 'package:bio_clean/screen/task/add_task.dart';
import 'package:bio_clean/utils/helpers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TaskScreen extends StatefulWidget {

  @override
  _TaskScreenState createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen> with Helpers{

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: Container(
              margin: EdgeInsetsDirectional.only(
                  top: SizeConfig().scaleWidth(0),
                  bottom: SizeConfig().scaleWidth(0),
                  start: SizeConfig().scaleWidth(5)),
              child: IconButton(
                icon: Icon(Icons.arrow_back_ios_rounded),
                onPressed: () {
                  Navigator.pushReplacementNamed(context, 'home_screen');
                },
              )),          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(20),
                bottom: SizeConfig().scaleWidth(0),
                end: SizeConfig().scaleWidth(0)),
            child: Text(
              'TASKS',
              style: TextStyle(
                  letterSpacing: 2,
                  wordSpacing: 0.5,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig().scaleWidth(24)),
            ),
          ),
        ),
        body: Stack(
          children: [
            Container(
              clipBehavior: Clip.antiAlias,
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: AlignmentDirectional.topStart,
                  end: AlignmentDirectional.bottomEnd,
                  colors: [
                    Color(0xffED2424),
                    Colors.white,
                  ],
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(140),
            ),
            Align(
              child: Container(
                clipBehavior: Clip.antiAlias,
                margin: EdgeInsets.only(top: 100),
                width: 414,
                height: double.infinity,
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                  color: Color(0xffF0F4FD),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
                child: StreamBuilder<QuerySnapshot>(
                    stream: FbFireStoreController().readTask(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        List<DocumentSnapshot> documents = snapshot.data!.docs;
                        return Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 24, vertical: 0),
                          child: ListView.separated(
                            padding: EdgeInsetsDirectional.only(top: 50),
                            itemBuilder: (context, index) {
                              return InkWell(
                                onLongPress: (){
                                  showDialogFunction(document: documents[index].id);
                                },
                                child: showItem(
                                  icon: documents[index].get('done') == '0' ? Icons.task : Icons.task_alt_outlined,
                                  documents: documents,
                                  index: index,
                                  context: context,
                                  title: documents[index].get('name'),
                                  subtitle: documents[index]
                                      .get('description')
                                      .toString(),
                                  date: documents[index]
                                      .get('created')
                                      .toString(),
                                ),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => EditTask(
                                          task: getTask(documents[index]),
                                        ),
                                      ));
                                },
                              );
                            },
                            separatorBuilder: (BuildContext context, int index) {
                              return SizedBox(
                                height: SizeConfig().scaleHeight(14),
                              );
                            },
                            itemCount: documents.length,
                            shrinkWrap: false,
                            physics: BouncingScrollPhysics(),
                            clipBehavior: Clip.antiAlias,
                          ),
                        );
                      } else {
                        return Center(
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.warning_amber_outlined,
                                  color: Colors.grey,
                                  size: 50,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  'NO TASKS',
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }
                    }),
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AddTask(),
                ));
          },
          child: Icon(Icons.add),
          backgroundColor: Color(0xffED2424),
        ),
      ),
    );
  }





  Future<void> delete({required String path}) async {
    bool status = await FbFireStoreController().deleteTask(path: path);
    if (status) {
      showSnackBar(context: context, message: 'Task Deleted Successfully');
    }
  }

  Task getTask(DocumentSnapshot snapshot) {
    Task task = Task();
    task.path = snapshot.id;
    task.name = snapshot.get('name');
    task.date = snapshot.get('date');
    // task.email = snapshot.get('email');
    task.time = snapshot.get('time');
    task.created = snapshot.get('created');
    task.description = snapshot.get('description');
    task.marketName = snapshot.get('marketName');
    task.done = snapshot.get('done');
    return task;
  }
  void showDialogFunction({required String document}) {
    showDialog(
        barrierColor: Colors.black.withOpacity(0.16),
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Confirm Delete'),
            content: Text('Are Your Sure!'),
            actions: [
              TextButton(
                  onPressed: ()  {
                    Navigator.pop(context);
                    delete(path: document);
                  },
                  child: Text('YES')),
              TextButton(
                  onPressed: () => Navigator.pop(context), child: Text('NO')),
            ],
// actionsPadding: EdgeInsets.symmetric(horizontal: 50),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
          );
        });
  }

}
