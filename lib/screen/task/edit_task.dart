import 'package:bio_clean/firebase/fb_firestore.dart';
import 'package:bio_clean/mod/task.dart';
import 'package:bio_clean/mod/team.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:bio_clean/utils/helpers.dart';
import 'package:bio_clean/widgets/app_text_filed.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class EditTask extends StatefulWidget {
  final Task? task;

  EditTask({required this.task});

  @override
  _EditTaskState createState() => _EditTaskState();
}

class _EditTaskState extends State<EditTask> with Helpers {
  late var _taskNameController;
  late var _taskTimeController;
  late var _taskDateController;
  late var _taskDescriptionController;
  late Color borderColor = Color(0x6fed2424);
  List<Team> teem = <Team>[];
  List<Team> employeeName = <Team>[];
  List<Team> teemSelected = <Team>[];
  String? name;
  Color color = Color(0xffED2424);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _taskNameController = TextEditingController(text: widget.task!.name);
    _taskTimeController = TextEditingController(text: widget.task!.time);
    _taskDateController = TextEditingController(text: widget.task!.date);
    _taskDescriptionController = TextEditingController(text: widget.task!.description);
    getCustomerName();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _taskNameController.dispose();
    _taskTimeController.dispose();
    _taskDateController.dispose();
    _taskDescriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          actions: [
            IconButton(
              onPressed: () {
                save();
              },
              icon: Icon(
                Icons.check_outlined,
                color: Colors.white,
              ),
            )
          ],
          centerTitle: true,
          elevation: 0,
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          title: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(20),
                bottom: SizeConfig().scaleWidth(10),
                end: SizeConfig().scaleWidth(0)),
            child: Text(
              ' EDIT TASK',
              style: TextStyle(
                  letterSpacing: 2,
                  wordSpacing: 0.5,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig().scaleWidth(24)),
            ),
          ),
        ),
        body: Stack(
          children: [
            Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: AlignmentDirectional.topStart,
                  end: AlignmentDirectional.bottomEnd,
                  colors: [
                    Color(0xffED2424),
                    Colors.white,
                  ],
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(140),
            ),
            Align(
              child: Container(
                margin: EdgeInsets.only(top: SizeConfig().scaleHeight(100)),
                width: 414,
                height: double.infinity,
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                  color: Color(0xffF0F4FD),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          SizedBox(
                            height: SizeConfig().scaleHeight(40),
                          ),
                          AppTextFiled(
                            labelText: 'Task Name',
                            controller: _taskNameController,
                            prefix: Icons.title,
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            textInputType: TextInputType.datetime,
                            labelText: 'Task Time',
                            // isEnabled: false,
                            controller: _taskTimeController,
                            prefix: Icons.timer,
                            onTap: () {
                              showTimePicker(
                                context: context,
                                initialTime: TimeOfDay.now(),
                              ).then((value) => _taskTimeController.text =
                                  value!.format(context).toString());
                            },
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            textInputType: TextInputType.datetime,
                            labelText: 'Task Date',
                            // isEnabled: false,
                            controller: _taskDateController,
                            prefix: Icons.date_range_rounded,
                            onTap: () {
                              showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime.now(),
                                lastDate:
                                    DateTime.now().add(Duration(days: 60)),
                              ).then((value) {
                                // dateController.text=value.toString();
                                //باقي الشغل
                                _taskDateController.text = DateFormat.yMMMd()
                                    .format(value!)
                                    .toString();
                                // print(DateFormat.yMMMd().format(value!));
                              });
                            },
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            labelText: 'Task Description',
                            controller: _taskDescriptionController,
                            prefix: Icons.description_rounded,
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          InkWell(
                            onTap: () {
                              showBottomSheet();
                            },
                            child: Container(
                              width: double.infinity,
                              height: 70,
                              padding: EdgeInsets.symmetric(horizontal: 0),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: teemSelected.length > 0
                                          ? color
                                          : borderColor,
                                      width: 2),
                                  color: Color(0xffF0F4FD),
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(10.0),
                                    bottomLeft: Radius.circular(10.0),
                                    bottomRight: Radius.circular(10.0),
                                    topLeft: Radius.circular(10.0),
                                  )),
                              child: employeeName.length < 0
                                  ? Text('data')
                                  : Container(
                                      margin: EdgeInsetsDirectional.only(
                                          start: 0, end: 0),
                                      child: SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        physics: BouncingScrollPhysics(),
                                        clipBehavior:
                                            Clip.antiAliasWithSaveLayer,
                                        padding: EdgeInsets.zero,
                                        child: Row(
                                          children: [
                                            Container(
                                                margin:
                                                    EdgeInsetsDirectional.only(
                                                        start: 10, end: 10),
                                                child: Row(
                                                  children: [
                                                    Icon(
                                                      Icons.person_pin,
                                                      color: Color(0xffED2424),
                                                      size: 25,
                                                    ),
                                                    SizedBox(
                                                      width: SizeConfig()
                                                          .scaleWidth(10),
                                                    ),
                                                    Text(
                                                      'Select Employee',
                                                      style: TextStyle(
                                                          color:
                                                              Color(0xffED2424),
                                                          fontSize: 16),
                                                    ),
                                                    widget.task != null
                                                        ? ListView.separated(
                                                            padding:
                                                                EdgeInsetsDirectional
                                                                    .zero,
                                                            physics:
                                                                BouncingScrollPhysics(),
                                                            shrinkWrap: true,
                                                            itemCount: widget
                                                                .task!
                                                                .marketName
                                                                .length,
                                                            scrollDirection:
                                                                Axis.horizontal,
                                                            itemBuilder:
                                                                (context,
                                                                    index) {
                                                              return Column(
                                                                mainAxisSize:
                                                                    MainAxisSize
                                                                        .min,
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .center,
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .center,
                                                                children: [
                                                                  CircleAvatar(
                                                                    child: Icon(
                                                                      Icons
                                                                          .person_pin,
                                                                      color: Colors
                                                                          .white,
                                                                      size: 20,
                                                                    ),
                                                                    backgroundColor:
                                                                        Color(
                                                                            0xffED2424),
                                                                    radius: 15,
                                                                  ),
                                                                  SizedBox(
                                                                    height: 5,
                                                                  ),
                                                                  Text(
                                                                    //here work to show shop name
                                                                    widget.task!
                                                                            .marketName[
                                                                        index],
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .black,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w500,
                                                                        fontSize:
                                                                            14),
                                                                  ),
                                                                ],
                                                              );
                                                            },
                                                            separatorBuilder:
                                                                (BuildContext
                                                                        context,
                                                                    int index) {
                                                              return SizedBox(
                                                                width: SizeConfig()
                                                                    .scaleWidth(
                                                                        10),
                                                              );
                                                            },
                                                          )
                                                        : Text('')
                                                  ],
                                                )),
                                            SizedBox(
                                              width:
                                                  SizeConfig().scaleWidth(20),
                                            ),
                                            ListView.separated(
                                              padding:
                                                  EdgeInsetsDirectional.zero,
                                              physics: BouncingScrollPhysics(),
                                              shrinkWrap: true,
                                              itemCount: teemSelected.length,
                                              scrollDirection: Axis.horizontal,
                                              itemBuilder: (context, index) {
                                                return Column(
                                                  mainAxisSize:
                                                      MainAxisSize.max,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    CircleAvatar(
                                                      child: Icon(
                                                        Icons.person_pin,
                                                        color: Colors.white,
                                                        size: 20,
                                                      ),
                                                      backgroundColor:
                                                          Color(0xffED2424),
                                                      radius: 15,
                                                    ),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    Text(
                                                      //here work to show shop name
                                                      teemSelected[index].name,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: 12),
                                                    ),
                                                  ],
                                                );
                                              },
                                              separatorBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return SizedBox(
                                                  width: SizeConfig()
                                                      .scaleWidth(10),
                                                );
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void showBottomSheet() {
    showModalBottomSheet(
        clipBehavior: Clip.antiAlias,
        barrierColor: Colors.black.withOpacity(0.25),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        context: context,
        builder: (context) {
          return Padding(
              padding: EdgeInsets.symmetric(vertical: 15),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: StatefulBuilder(
                      builder: (BuildContext context,
                          void Function(void Function()) setState) {
                        return ListView.builder(
                          itemCount: teem.length,
                          itemBuilder: (context, index) {
                            return CheckboxListTile(
                              title: Text(teem[index].name),
                              value: teem[index].states,
                              onChanged: (status) {
                                setState(() {
                                  teem[index].states = status!;
                                });
                                print(status);
                              },
                            );
                          },
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(16),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize:
                            Size(double.infinity, SizeConfig().scaleHeight(60)),
                        primary: Color(0xffED2424),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      onPressed: () {
                        setState(() {
                          teemSelected.clear();
                          for (Team teem in teem) {
                            if (teem.states) {
                              teemSelected.add(teem);
                              setState(() {
                                if (teemSelected.length > 0) {
                                  setState(() {
                                    color = color;
                                  });
                                } else {
                                  setState(() {
                                    color = Colors.red;
                                  });
                                }
                              });
                            }
                          }
                        });
                        Navigator.pop(context);
                      },
                      child: Text(
                        'Done',
                        style:
                            TextStyle(fontSize: SizeConfig().scaleTextFont(20)),
                      ),
                    ),
                  ),
                ],
              ));
        });
  }

  bool checkData() {
    if (_taskNameController.text.isNotEmpty &&
        _taskTimeController.text.isNotEmpty &&
        _taskDateController.text.isNotEmpty &&
        _taskDescriptionController.text.isNotEmpty &&
        teemSelected.length != 0) {
      return true;
    } else {
      showSnackBar(
          context: context, message: 'Enter Required Data', error: true);
      return false;
    }
  }

  Future<void> save() async {
    if (checkData()) {
      if (widget.task != null) {
        await updateTask();
      } else {
        await addTask();
      }
    }
  }

  Future<void> addTask() async {
    bool state = await FbFireStoreController().createTask(task: task);
    if (state) {
      Navigator.pop(context);
    }
  }

  Future<void> updateTask() async {
    bool state = await FbFireStoreController()
        .updateTask(path: widget.task!.path, task: taskUpdate);
    if (state) {
      Navigator.pop(context);
    }
  }

  void clear() {
    _taskNameController.text = '';
    _taskTimeController.text = '';
    _taskDateController.text = '';
    _taskDescriptionController.text = '';
  }

  Task getTask(DocumentSnapshot snapshot) {
    Task task = Task();
    task.path = snapshot.id;
    task.name = snapshot.get('name');
    task.date = snapshot.get('date');
    task.email = snapshot.get('email');
    task.time = snapshot.get('time');
    task.created = snapshot.get('created');
    task.marketName = snapshot.get('marketName');
    task.description = snapshot.get('description');
    task.done = snapshot.get('done');
    return task;
  }

  Task get task {
    Task task = Task();
    task.name = _taskNameController.text;
    task.time = _taskTimeController.text;
    task.date = _taskDateController.text;
    task.description = _taskDescriptionController.text;
    task.done = '0';
    for (int i = 0; i < teemSelected.length; i++) {
      print(teemSelected[i].name);
      task.marketName.add(teemSelected[i].name);
      task.email.add(teemSelected[i].email);
    }
    return task;
  }
  Task get taskUpdate {
    Task task = Task();
    task.name = _taskNameController.text;
    task.time = _taskTimeController.text;
    task.date = _taskDateController.text;
    task.description = _taskDescriptionController.text;
    task.done = '0';
    for (int i = 0; i < teemSelected.length; i++) {
      print(teemSelected[i].name);
      task.marketName.add(teemSelected[i].name);
      // task.email.add(teemSelected[i].email);
    }
    return task;
  }

  Future<void> getCustomerName() async {
    List<String> names = await FbFireStoreController().getMarketName();
    for (int i = 0; i < names.length; i++) {
      print(names.length);
      setState(() {
        teem.add(Team.name(name: names[i], states: false,));
      });
    }
  }
}
