import 'package:bio_clean/preferences/student_pref.dart';
import 'package:bio_clean/screen/auth/change_password.dart';
import 'package:bio_clean/screen/auth/foreget_password_screen.dart';
import 'package:bio_clean/screen/auth/home_screen.dart';
import 'package:bio_clean/screen/auth/launch_screen.dart';
import 'package:bio_clean/screen/auth/sign_up_screen.dart';
import 'package:bio_clean/screen/employee/employee_screen.dart';
import 'package:bio_clean/screen/login/login_screen.dart';
import 'package:bio_clean/screen/meet/add_meet.dart';
import 'package:bio_clean/screen/meet/meeet_screen.dart';
import 'package:bio_clean/screen/profile/profile_screen.dart';
import 'package:bio_clean/screen/task/task_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppPreferences().initPreferences();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Locale _locale = Locale('en');
    return MaterialApp(
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        appBarTheme: AppBarTheme(
          systemOverlayStyle: SystemUiOverlayStyle(
              statusBarColor: Color(0x6fed2424),
              statusBarBrightness: Brightness.light,
              statusBarIconBrightness: Brightness.light),
        ),
      ),
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        Locale('en'),
      ],
      locale: _locale,
      initialRoute: 'launch_screen',
      // home: LoginScreen(),

      routes: {
        'launch_screen': (context) => LaunchScreen(),
        'login_screen': (context) => LoginScreen(),
        'home_screen': (context) => HomeScreen(),
        'crate_account': (context) => SignUpScreen(),
        'forgot_password': (context) => ForgetPasswordScreen(),
        'add_meet': (context) => AddMeetScreen(),
        'meet_screen': (context) => MeetScreen(),
        // 'add_task': (context) => AddTask(),
        'task_screen': (context) => TaskScreen(),
        'employee_screen': (context) => EmployeeScreen(),
        // 'add_employee': (context) => AddEmployeeScreen(),
        'profile_screen': (context) => ProfileScreen(),
        'change_password': (context) => ChangePassword(),
      },
    );
  }
}
