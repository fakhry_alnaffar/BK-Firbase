class Task {
  late String path = '';
  late String name='';
  late String description='';
  late String time='';
  late String date='';
  late String updatedAt='';
  String created = DateTime.now().toString().split(' ')[0];
  List <String> email = <String>[];
  String done = '';
  List<dynamic> marketName = <dynamic>[];

  Task();

  Task.forMap(Map<String, dynamic> map) {
    name = map['name'];
    marketName = map['marketName'];
    created = map['created'];
    email = map['email'];
    description = map['description'];
    time = map['time'];
    done = map['done'];
    date = map['date'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['name'] = name;
    map['marketName'] = marketName;
    map['created'] = created;
    map['email'] = email;
    map['done'] = done;
    map['time'] = time;
    map['description'] = description;
    map['date'] = date;
    return map;
  }
}
