class Employee{
  String name = '';
  String mobileNumber = '';
  String email = '';
  String path = '';
  String image = '';

  Employee();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['name'] = name;
    map['mobileNumber'] = mobileNumber;
    map['email'] = email;
    map['image'] = image;
    return map;
  }
}