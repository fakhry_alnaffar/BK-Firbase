class Admin{
  String name = '';
  String email = '';
  String image = '';
  String path = '';

  Admin();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['name'] = name;
    map['email'] = email;
    map['image'] = image;
    map['path'] = path;
    return map;
  }
}