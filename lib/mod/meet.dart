class Meet {
  late String path='';
  late String? name;
  late String? description;
  late String? time;
  late String? date;
  String done = '';
  String created = DateTime.now().toString().split(' ')[0];
  late String updatedAt='';
  Meet();



  Meet.forMap(Map<String, dynamic> map) {
    name = map['name'];
    description = map['description'];
    time = map['time'];
    date = map['date'];
    done = map['done'];
    created = map['created'];
    updatedAt = map['updated_at'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['name'] = this.name;
    map['description'] = this.description;
    map['time'] = this.time;
    map['date'] = this.date;
    map['done'] = this.done;
    map['created'] = created;
    map['updated_at'] = this.updatedAt;
    return map;
  }
}