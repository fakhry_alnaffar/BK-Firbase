class Customers{
  String name = '';
  String marketName = '';
  String address = '';
  String phoneNumber = '';
  String path = '';

  Customers();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['name'] = name;
    map['marketName'] = marketName;
    map['address'] = address;
    map['phoneNumber'] = phoneNumber;
    return map;
  }
}