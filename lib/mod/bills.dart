class Bill{
  String marketName = '';
  String marketId = '';
  String product = '';
  String amountPaid = '';
  String totalPrice = '';
  String remainingAmount = '';
  String created = DateTime.now().toString().split(' ')[0];
  String path = '';
  String email = '';
  String done = '';

  Bill();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['marketName'] = marketName;
    map['marketId'] = marketId;
    map['product'] = product;
    map['amountPaid'] = amountPaid;
    map['totalPrice'] = totalPrice;
    map['remainingAmount'] = remainingAmount;
    map['created'] = created;
    map['path'] = path;
    map['email'] = email;
    map['done'] = done;
    return map;
  }
}