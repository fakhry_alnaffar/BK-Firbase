
class OrderItem{
  String name = '';
  int quantity = 0;
  int price = 0;
  int total = 0;

  OrderItem({required this.name, required this.quantity, required this.price, required this.total});
}