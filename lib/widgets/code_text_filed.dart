import 'package:flutter/material.dart';

class CodeTextField extends StatelessWidget {
  final FocusNode focusNode;
  final TextEditingController textEditingControoler;
  final void Function(String value)onChanged;


  CodeTextField({
   required this.focusNode,
   required this.textEditingControoler,
   required this.onChanged});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: TextField(
          maxLength: 1,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 18
          ),
          keyboardType: TextInputType.number,
          focusNode: focusNode,
          controller: textEditingControoler,
          decoration: InputDecoration(
            counterText: '',
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(width: 1, color: Colors.blue),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(width: 1, color: Colors.grey.shade500),
            ),
          ),
          onChanged:onChanged,
      ),
    );
  }
}
